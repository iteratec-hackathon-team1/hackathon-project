package de.iteratechackathon.team1;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;

//import jdk.internal.org.objectweb.asm.Label;

public class GUI extends JFrame {

	/**
	 * 
	 * Serial ID
	 * 
	 */
	private static final long serialVersionUID = 2390070784636505825L;
	private static JTextArea txta_Input;
	private JLabel icon;
	private JPanel panel;

	public static String getText() {
		return txta_Input.getText();
	}

	public GUI(ActionListener buttonMethod, ActionListener restart) {

		this.setName("Team 1");
		this.setSize(800, 350);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		initComponents(buttonMethod, restart);

		this.setVisible(true);
	}

	private void initComponents(ActionListener buttonMethod, ActionListener restart) {
		panel = new JPanel();

		txta_Input = new JTextArea();
		txta_Input.setPreferredSize(new Dimension(400, 200));
		txta_Input.setLocation(0, 0);

		JLabel label1 = new JLabel();
		label1.setText("Bewertung eingeben:");

		JLabel label2 = new JLabel();
		label2.setText("Deine Bewertung würde ergeben:");

		JButton button = new JButton();
		button.setText("Berechnen");
		button.addActionListener(buttonMethod);

		JButton button2 = new JButton();
		button2.setText("Nochmal versuchen");
		button2.addActionListener(restart);

		icon = new JLabel();

		panel.add(label1);
		panel.add(txta_Input);
		panel.add(button2);
		panel.add(button);
		panel.add(label2);
		panel.add(icon);

		this.add(panel);

	}

	public void addStars(String i) {
		int stars = 0;
		try {
			stars = Integer.parseInt(i);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		for (int x = 0; x < stars; x++) {
			addStar();
		}
	}

	public void addStar() {
		JLabel icon = new JLabel();
		icon.setPreferredSize(new Dimension(50, 50));
		BufferedImage img = null;
		try {
			// img = ImageIO.read(new File("gui/daumen-hoch.jpg"));
			img = ImageIO.read(new File("gui/stern.png"));
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("gui/daumen-hoch.jpg");
		}

		Image dimg = img.getScaledInstance(30, 30, Image.SCALE_SMOOTH);

		ImageIcon imageIcon = new ImageIcon(dimg);

		icon.setIcon(imageIcon);

		panel.add(icon);
		panel.revalidate();
	}

	public void setUp() {
		setIcon("gui/daumen-hoch.jpg", true);
		System.out.println("hoch");
	}

	public void setDown() {
		setIcon("gui/daumen-hoch.jpg", false);
		System.out.println("down");
	}

	public void setNone() {
		icon.setIcon(null);
	}

	public String getInput() {
		return txta_Input.getText();
	}

	private void setIcon(String path, boolean up) {
		icon.setPreferredSize(new Dimension(50, 50));
		BufferedImage img = null;
		try {
			img = ImageIO.read(new File(path));
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println(path);
		}

		if (!up) {
			img = rotateCw(img);
			img = rotateCw(img);
		}

		Image dimg = img.getScaledInstance(30, 30, Image.SCALE_SMOOTH);

		ImageIcon imageIcon = new ImageIcon(dimg);

		icon.setIcon(imageIcon);
	}

	public static BufferedImage rotateCw(BufferedImage img) {
		int width = img.getWidth();
		int height = img.getHeight();
		BufferedImage newImage = new BufferedImage(height, width, img.getType());

		for (int i = 0; i < width; i++)
			for (int j = 0; j < height; j++)
				newImage.setRGB(height - 1 - j, i, img.getRGB(i, j));

		return newImage;
	}

}
