# Iteratec-Hackathon

A simple sentiment analysis software that assigns a 1-5 star rating to a product review.

## Libraries

- [Mallet (MAchine Learning for LanguagE Toolkit)](http://mallet.cs.umass.edu)
    - [download to](http://mallet.cs.umass.edu/dist/mallet-2.0.8.zip) `mallet/`

## Data Sets

- [sentiment labelled sentences](https://archive.ics.uci.edu/ml/datasets/Sentiment+Labelled+Sentences)
    - [download](https://archive.ics.uci.edu/ml/machine-learning-databases/00331/sentiment%20labelled%20sentences.zip) to `data-sets/`
    - data
        - kurze bewertung
        - 0/1 für positiv/negativ
- [Large Movie Review Dataset](http://ai.stanford.edu/~amaas/data/sentiment/)
    - [download](http://ai.stanford.edu/~amaas/data/sentiment/aclImdb_v1.tar.gz)
    - every review is a separate file
- [Huge Amazon (and more) Dataset](https://www.kaggle.com/bittlingmayer/amazonreviews)
    - [Google Drive](https://drive.google.com/drive/folders/0Bz8a_Dbh9Qhbfll6bVpmNUtUcFdjYmF2SEpmZUZUcVNiMUw1TWN6RDV3a0JHT3kxLVhVR2M)