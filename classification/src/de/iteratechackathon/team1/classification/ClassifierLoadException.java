package de.iteratechackathon.team1.classification;

class ClassifierLoadException extends Exception {
    ClassifierLoadException(String message) {
        super(message);
    }
}
