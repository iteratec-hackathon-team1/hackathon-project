package de.iteratechackathon.team1.classification;

import cc.mallet.classify.Classifier;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;


class ClassifierLoader {

    static Classifier loadClassifier(String classifierLocation) throws ClassifierLoadException {
        Classifier classifier;

        try (FileInputStream fis = new FileInputStream(classifierLocation)) {
            try (ObjectInputStream ois = new ObjectInputStream(fis)) {

                // read classifier
                classifier = (Classifier) ois.readObject();

            } catch (ClassNotFoundException e) {
                throw new ClassifierLoadException("Could not find class");
            }
        } catch (FileNotFoundException e) {
            throw new ClassifierLoadException("Could not find classifier");
        } catch (IOException e) {
            throw new ClassifierLoadException("Could not read classifier");
        }

        return classifier;
    }

}
