package de.iteratechackathon.team1.classification;

import cc.mallet.classify.Classifier;
import cc.mallet.pipe.iterator.ArrayIterator;
import cc.mallet.types.Labeling;

import java.util.Iterator;

public class Main {

    static String CLASSIFIER_LOCATION = "training/output/classifier";

    public static void main(String[] args) {
//        if (args.length == 0) {
//            System.err.println("No input text given");
//            return;
//        }
//        String inputText = args[0];
        String inputText = "nice";

        String prediction;
        try {
            prediction = classify(inputText);
        } catch (ClassificationException e) {
            return;
        }

        System.out.println("Prediction: " + prediction);
    }


    public static String classify(String text) throws ClassificationException {
        // read classifier
        Classifier classifier;
        try {
            classifier = ClassifierLoader.loadClassifier(CLASSIFIER_LOCATION);
        } catch (ClassifierLoadException e) {
            System.err.println(e.getMessage());
            throw new ClassificationException();
        }


        String[] data = new String[] { text };
        ArrayIterator iterator = new ArrayIterator(data);
        // Create an iterator that will pass each instance through
        // the same pipe that was used to create the training data
        // for the classifier.
        Iterator instances = classifier.getInstancePipe().newIteratorFrom(iterator);


        // Classifier.classify() returns a Classification object
        // that includes the instance, the classifier, and the
        // classification results (the labeling). Here we only
        // care about the Labeling.
        Labeling labeling = classifier.classify(instances.next()).getLabeling();
        return labeling.getBestLabel().toString();
    }

}
