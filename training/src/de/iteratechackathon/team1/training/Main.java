package de.iteratechackathon.team1.training;


import cc.mallet.classify.Classifier;
import cc.mallet.classify.ClassifierTrainer;
import cc.mallet.classify.MaxEntTrainer;
import cc.mallet.types.InstanceList;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.stream.Stream;


public class Main {

    private static String CLASSIFIER_TARGET_LOCATION = "training/output/classifier";

    public static void main(String[] args) {
        Classifier classifier = trainClassifier();

        try {
            exportClassifier(classifier);
        } catch (IOException e) {
            System.err.println("Classifier could not be written to disk");
        }
    }


    private static Classifier trainClassifier() {
        InstanceList trainingInstances = DataImporter.parseRawData();

        // use the maximum entropy classifier
        ClassifierTrainer trainer = new MaxEntTrainer();
        return trainer.train(trainingInstances);
    }


    private static void exportClassifier(Classifier classifier) throws IOException {
        FileOutputStream fos = new FileOutputStream(CLASSIFIER_TARGET_LOCATION);
        ObjectOutputStream oos = new ObjectOutputStream(fos);

        oos.writeObject(classifier);

        fos.close();
        oos.close();
    }

}
