package de.iteratechackathon.team1.training.binary;

import cc.mallet.pipe.iterator.CsvIterator;
import cc.mallet.types.InstanceList;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class CsvParser {

    private static final String[] RAW_DATA_FILE_NAMES = {
            "training/data-sets/sentiment labelled sentences/sentiment labelled sentences/amazon_cells_labelled.txt",
            "training/data-sets/sentiment labelled sentences/sentiment labelled sentences/imdb_labelled.txt",
            "training/data-sets/sentiment labelled sentences/sentiment labelled sentences/yelp_labelled.txt",
    };

    private static final File[] RAW_DATA_FILES = Stream.of(RAW_DATA_FILE_NAMES).map(File::new).toArray(File[]::new);


    // Pattern used to parse each line
    private static final Pattern LINE_PATTERN = Pattern.compile("([^\\t]+)\\t+([01])");
    // The first position in the regular expression is data
    private static final int DATA_INDEX = 1;
    // the second position is the target group (0/1 for negative/positive)
    private static final int TARGET_INDEX = 2;
    // use the data as name
    private static final int NAME_INDEX = 1;



    public static void addInstancesTo(InstanceList instances) {
        for (File file : RAW_DATA_FILES) {
            FileReader reader;
            try {
                reader = new FileReader(file);
            } catch (FileNotFoundException e) {
                System.err.println("Could not find file \"" + file.getAbsolutePath() + "\"");
                continue;
            }

            CsvIterator iterator = new CsvIterator(reader, LINE_PATTERN, DATA_INDEX, TARGET_INDEX, NAME_INDEX);

            // Now process each instance provided by the iterator.
            instances.addThruPipe(iterator);
        }
    }
}
