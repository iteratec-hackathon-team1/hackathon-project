package de.iteratechackathon.team1.training.binary;

import java.io.File;
import java.io.FileFilter;


/**
 * This class illustrates how to build a simple file filter
 */
class TxtFilter implements FileFilter {

    /**
     * Test whether the string representation of the file
     * ends with the correct extension. Note that FileIterator
     * will only call this filter if the file is not a directory,
     * so we do not need to test that it is a file.
     */
    public boolean accept(File file) {
        return file.toString().endsWith(".txt");
    }
}
