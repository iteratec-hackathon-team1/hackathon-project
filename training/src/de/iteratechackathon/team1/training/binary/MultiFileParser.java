package de.iteratechackathon.team1.training.binary;

import java.io.File;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import cc.mallet.pipe.iterator.FileIterator;
import cc.mallet.types.InstanceList;


public class MultiFileParser {

    private static final Pattern FILE_REGEX = Pattern.compile("\\D*([01]).*");

    private static final String[] RAW_DATA_DIR_NAMES = {
        "training/data-sets/aclImdb/"
    };

    private static final File[] RAW_DATA_DIRS = Stream.of(RAW_DATA_DIR_NAMES).map(File::new).toArray(File[]::new);

    public static void addInstancesTo(InstanceList instances) {
        //  Construct a file iterator, starting with the
        //  specified directories, and recursing through subdirectories.

        // The second argument specifies a FileFilter to use to select
        //  files within a directory.

        // The third argument is a Pattern that is applied to the
        //   filename to produce a class label. In this case, I've 
        //   asked it to use the last directory name in the path.

        FileIterator iterator = new FileIterator(RAW_DATA_DIRS, new TxtFilter(), FILE_REGEX);

        // Now process each instance provided by the iterator.
        instances.addThruPipe(iterator);
    }

}
