package de.iteratechackathon.team1.training.rating;

import cc.mallet.pipe.iterator.FileIterator;
import cc.mallet.types.InstanceList;
import cc.mallet.util.RegexFileFilter;

import java.io.File;
import java.util.regex.Pattern;
import java.util.stream.Stream;


public class MultiFileParser {

    private static final Pattern FILE_REGEX = Pattern.compile("\\D*[01]\\D*\\d*_(\\d)\\.txt");

    private static final String[] RAW_DATA_DIR_NAMES = {
        "training/data-sets/aclImdb/"
    };

    private static final File[] RAW_DATA_DIRS = Stream.of(RAW_DATA_DIR_NAMES).map(File::new).toArray(File[]::new);

    public static void addInstancesTo(InstanceList instances) {
        // Construct a file iterator, starting with the
        //  specified directories, and recursing through subdirectories.
        // The second argument specifies a FileFilter to use to select
        //  files within a directory.
        // The third argument is a Pattern that is applied to the
        //   filename to produce a class label.
        FileIterator iterator = new FileIterator(RAW_DATA_DIRS, new RegexFileFilter(FILE_REGEX), FILE_REGEX);

        // Now process each instance provided by the iterator.
        instances.addThruPipe(iterator);
    }

}
