package de.iteratechackathon.team1.training.rating;

import cc.mallet.pipe.iterator.CsvIterator;
import cc.mallet.types.InstanceList;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class CsvParser {

    private static final String[] RAW_DATA_FILE_NAMES = {
            "training/data-sets/amazon_review_full_csv/test.csv",
//            "training/data-sets/amazon_review_full_csv/train.csv",
//            "training/data-sets/yelp_review_full_csv/test.csv",
//            "training/data-sets/yelp_review_full_csv/train.csv",
    };

    private static final File[] RAW_DATA_FILES = Stream.of(RAW_DATA_FILE_NAMES).map(File::new).toArray(File[]::new);


    // Pattern used to parse each line
    private static final Pattern LINE_PATTERN_A = Pattern.compile("\"(\\d)\",\"(.*)\",\"(.*)\"");
    private static final Pattern LINE_PATTERN_B = Pattern.compile("\"(\\d)\",\"(.*)\"");


    public static void addInstancesTo(InstanceList instances) {
        for (File file : RAW_DATA_FILES) {
            FileReader reader;
            try {
                reader = new FileReader(file);
            } catch (FileNotFoundException e) {
                System.err.println("Could not find file \"" + file.getAbsolutePath() + "\"");
                continue;
            }

            // The first position in the regular expression is data
            int dataIndex;
            // the second position is the target group (0/1 for negative/positive)
            int targetIndex;
            // name of the data set
            int nameIndex;
            Pattern pattern;
            if (file.getName().contains("yelp")) {
                pattern = LINE_PATTERN_B;
                dataIndex = 2;
                targetIndex = 1;
                nameIndex = 2;
            } else {
                pattern = LINE_PATTERN_A;
                dataIndex = 3;
                targetIndex = 1;
                nameIndex = 2;
            }
            CsvIterator iterator = new CsvIterator(reader, pattern, dataIndex, targetIndex, nameIndex);

            // Now process each instance provided by the iterator.
            instances.addThruPipe(iterator);
        }
    }
}
