package de.iteratechackathon.team1.training;

import cc.mallet.pipe.Pipe;
import cc.mallet.types.InstanceList;

import cc.mallet.pipe.*;
import de.iteratechackathon.team1.training.rating.CsvParser;
import de.iteratechackathon.team1.training.rating.MultiFileParser;

import java.util.ArrayList;
import java.util.regex.Pattern;



public class DataImporter {


    static InstanceList parseRawData() {
        // Construct a new instance list, passing it the pipe
        //  we want to use to process instances.
        InstanceList instances = new InstanceList(buildPipe());

        CsvParser.addInstancesTo(instances);
        MultiFileParser.addInstancesTo(instances);

        return instances;
    }



    // Regular expression for what constitutes a token. Examples:
    //    "\\S+"   (anything not whitespace)
    //    "\\w+"    ( A-Z, a-z, 0-9, _ )
    //    "[\\p{L}\\p{N}_]+|[\\p{P}]+"   (a group of only letters and numbers OR
    //                                    a group of only punctuation marks)
    private static final Pattern TOKEN_PATTERN = Pattern.compile("\\S+");

    private static Pipe buildPipe() {
        ArrayList pipeList = new ArrayList();

        // Read data from File objects
        pipeList.add(new Input2CharSequence("UTF-8"));

        // Tokenize raw strings
        pipeList.add(new CharSequence2TokenSequence(TOKEN_PATTERN));

        // Normalize all tokens to all lowercase
        pipeList.add(new TokenSequenceLowercase());

        // Remove stopwords from a standard English stoplist.
        pipeList.add(new TokenSequenceRemoveStopwords(false, false));

        // Rather than storing tokens as strings, convert
        // them to integers by looking them up in an alphabet.
        pipeList.add(new TokenSequence2FeatureSequence());

        // Do the same thing for the "target" field:
        // convert a class label string to a Label object,
        // which has an index in a Label alphabet.
        pipeList.add(new Target2Label());

        // Now convert the sequence of features to a sparse vector,
        // mapping feature IDs to counts.
        pipeList.add(new FeatureSequence2FeatureVector());

        // Print out the features and the label
        //pipeList.add(new PrintInputAndTarget());

        return new SerialPipes(pipeList);
    }

}
