package de.rest;


import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;


@Path("/test")
public class SimpleRestService {

	

	@GET
	@Path("/method")
    @Produces(MediaType.TEXT_PLAIN)
	public String getSomething(@QueryParam("comment") String comment) {

		System.out.println("test method");
        
        return comment;	
	}

	
}
